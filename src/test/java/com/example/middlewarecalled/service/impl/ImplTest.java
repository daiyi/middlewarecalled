package com.example.middlewarecalled.service.impl;


import com.example.middlewarecalled.bean.*;
import com.example.middlewarecalled.service.HttpClientSevice;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.util.internal.StringUtil;
import org.junit.Test;
import org.slf4j.Logger;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

public class ImplTest {

    private static final Logger logger = getLogger(ImplTest.class);
    SendTxServiceImpl sendTxService = new SendTxServiceImpl();
    private static final ObjectMapper mapper = new ObjectMapper();


    @Test
    public void sendTx() throws IOException {
        /*
        发送交易分为以下几步：
        1.编译合约
        2.获取块号
        3.构造部署合约请求
        4.签名
        5.发送部署合约
        6.发送交易及签名信息
         */
        //1.编译合约
        CompileSolidityReq compileSolidityReq = new CompileSolidityReq();
        compileSolidityReq.setSource("contract Token {   " +
                "  address issuer;   " +
                "  mapping (address => uint) balances;   " +
                "   event Issue(address account, uint amount);  " +
                "   event Transfer(address from, address to, uint amount);   " +
                "   function Token() {    " +
                "     issuer = msg.sender;  " +
                "   }   " +
                "   function issue(address account, uint amount) {     " +
                "    if (msg.sender != issuer) throw;     " +
                "    balances[account] += amount;     }   " +
                "   function transfer(address to, uint amount) {    " +
                "     if (balances[msg.sender] < amount) throw;    " +
                "      balances[msg.sender] -= amount;    " +
                "     balances[to] += amount;     " +
                "     Transfer(msg.sender, to, amount);     }   " +
                "   function getBalance(address account) constant returns (uint) {    " +
                "     return balances[account];     } }");
        /**
         * 该方法调用后会产生code,abi等信息
         */
        CommonResult<Map<String,CompileResult>> compileSolidityResult = sendTxService.compileSolidity(compileSolidityReq);
        String code = compileSolidityResult.getDetail().get("Token").getCode();
        String abi = compileSolidityResult.getDetail().get("Token").getAbiJson();
        Object[] params = new Object[0];
        //2.获取块号
        String bizId = (int) (Math.random() * 10000) + "";//获取bizId
        Integer currentBlockNumber = (Integer) sendTxService.blockNumber().getDetail();
        Integer blockLimit = currentBlockNumber + 100;
        //3.构造部署合约请求
        BuildDeployInfoReq buildDeployInfoReq = new BuildDeployInfoReq();
        buildDeployInfoReq.setBizId(bizId);
        buildDeployInfoReq.setBlockLimit(blockLimit.toString());
        buildDeployInfoReq.setCode(code);
        buildDeployInfoReq.setParams(params);
        buildDeployInfoReq.setJsonAbi(abi);
//        String.format("%s",abi);
        CommonResult buildDeployInfoReqResult = sendTxService.buildDeployInfo(buildDeployInfoReq);
        String hash = buildDeployInfoReqResult.getDetail().toString();
        //4.签名
        SignReq signReq = new SignReq();
        signReq.setMessageHash(hash);
        CommonResult signReqResult = sendTxService.sign(signReq);
        String txHash = signReqResult.getDetail().toString();
        //5.部署合约
        DeployReq deployReq = new DeployReq();
        deployReq.setSignatureInBase64(txHash);
        deployReq.setBizId(bizId);
        deployReq.setBlockLimit(blockLimit.toString());
        deployReq.setCode(code);
        deployReq.setParams(params);
        deployReq.setJsonAbi(abi);
        CommonResult deployReqResult = sendTxService.deploy(deployReq);
        String contractAddress = deployReqResult.getDetail().toString();
        logger.info("deployReqResult:{}",deployReqResult);
        //6.发送交易
        SendTxReq sendTxReq = new SendTxReq();
        sendTxReq.setBizId(bizId);
        sendTxReq.setBlockLimit(blockLimit.toString());
        sendTxReq.setContractAddress(contractAddress);
        sendTxReq.setSignatureInBase64(txHash);
        sendTxReq.setMethod("issue");
        sendTxReq.setParams(new Object[]{"0x75719430dab89c81e077007dc3a7f1292608c593",11});
        CommonResult sendTxReqResult = sendTxService.sendTx(sendTxReq);
        String sentTxResult = sendTxReqResult.getDetail().toString();
        logger.info("=====================sentTxResult:{}",sentTxResult);
    }
}
