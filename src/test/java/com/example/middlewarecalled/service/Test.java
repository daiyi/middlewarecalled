package com.example.middlewarecalled.service;

import com.alibaba.fastjson.JSON;
import com.example.middlewarecalled.bean.*;
import com.example.middlewarecalled.service.impl.SendTxServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

public class Test {
    private static final Logger logger = getLogger(Test.class);
//    @Autowired
//    private SendTxServiceImpl sendTxService;
    @org.junit.Test
    public void test1() {

        try {
            HttpClientSevice.get("http://localhost:9999/common/blockNumber");
        } catch (HttpException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 取当前同步块号
     *
     * @throws Exception
     */
    @org.junit.Test
    public void test2() throws Exception {
        String commonResult = HttpClientSevice.get("http://localhost:9999/common/blockNumberDB");
        logger.info("commonResult:{}", commonResult);
        CommonResult commonResult1 = JSON.parseObject(commonResult, new com.alibaba.fastjson.TypeReference<CommonResult>() {
        });
        logger.info("commonResult1:{}", commonResult1.toString());
    }

    /**
     * 编译合约
     *
     * @throws Exception
     */
    @org.junit.Test
    public void test3() throws Exception {
        CompileSolidityReq compileSolidityReq = new CompileSolidityReq();
        compileSolidityReq.setSource("contract Token {   " +
                "  address issuer;   " +
                "  mapping (address => uint) balances;   " +
                "   event Issue(address account, uint amount);  " +
                "   event Transfer(address from, address to, uint amount);   " +
                "   function Token() {    " +
                "     issuer = msg.sender;  " +
                "   }   " +
                "   function issue(address account, uint amount) {     " +
                "    if (msg.sender != issuer) throw;     " +
                "    balances[account] += amount;     }   " +
                "   function transfer(address to, uint amount) {    " +
                "     if (balances[msg.sender] < amount) throw;    " +
                "      balances[msg.sender] -= amount;    " +
                "     balances[to] += amount;     " +
                "     Transfer(msg.sender, to, amount);     }   " +
                "   function getBalance(address account) constant returns (uint) {    " +
                "     return balances[account];     } }");
        String result = HttpClientSevice.postJsonObject("http://localhost:9999/common/compileSolidity", compileSolidityReq);
        logger.info("commonResult:{}", result);

        CommonResult<Map<String,CompileResult>> commonResult = new CommonResult();
        ObjectMapper mapper = new ObjectMapper();
        commonResult = mapper.readValue(result,new  TypeReference<CommonResult<Map<String,CompileResult>>>(){});
        logger.info("commonResult:{}", commonResult.getDetail().get("Token").getAbiJson());
//        ObjectMapper mapper = new ObjectMapper();
//
//        commonResult = mapper.readValue(result,new  TypeReference<CommonResult<Map<String,CompileResult>>>(){});

    }

    /**
     * 构造一个部署合约的区块链请求
     *
     * @throws Exception
     */
    @org.junit.Test
    public void test4() throws Exception {
        String commonResult = HttpClientSevice.get("http://localhost:9999/common/blockNumberDB");
        logger.info("commonResult:{}", commonResult);
        CommonResult commonResult1 = JSON.parseObject(commonResult, new com.alibaba.fastjson.TypeReference<CommonResult>() {
        });
        Integer currentBlockNumber = (Integer) commonResult1.getDetail();
        Integer blockLinit = currentBlockNumber + 100;
        logger.info("commonResult1:{}", commonResult1.toString());
        BuildDeployInfoReq buildDeployInfoReq = new BuildDeployInfoReq();
        buildDeployInfoReq.setBizId((int) (Math.random() * 10000) + "");
        buildDeployInfoReq.setBlockLimit(blockLinit.toString());
        buildDeployInfoReq.setCode("0x60606040525b33600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908302179055505b6102c48061003f6000396000f360606040526000357c010000000000000000000000000000000000000000000000000000000090048063867904b41461004f578063a9059cbb14610070578063f8b2cb4f146100915761004d565b005b61006e60048080359060200190919080359060200190919050506100bd565b005b61008f600480803590602001909190803590602001909190505061015a565b005b6100a76004808035906020019091905050610286565b6040518082815260200191505060405180910390f35b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561011957610002565b80600160005060008473ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828282505401925050819055505b5050565b80600160005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005054101561019657610002565b80600160005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282825054039250508190555080600160005060008473ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828282505401925050819055507fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef338383604051808473ffffffffffffffffffffffffffffffffffffffff1681526020018373ffffffffffffffffffffffffffffffffffffffff168152602001828152602001935050505060405180910390a15b5050565b6000600160005060008373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000505490506102bf565b91905056");
        buildDeployInfoReq.setParams(new Object[0]);
        buildDeployInfoReq.setJsonAbi("[{\"outputs\":[],\"constant\":false,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"issue\",\"type\":\"function\"},{\"outputs\":[],\"constant\":false,\"inputs\":[{\"name\":\"to\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"type\":\"function\"},{\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"constant\":true,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"}],\"name\":\"getBalance\",\"type\":\"function\"},{\"inputs\":[],\"type\":\"constructor\"},{\"inputs\":[{\"indexed\":false,\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Issue\",\"anonymous\":false,\"type\":\"event\"},{\"inputs\":[{\"indexed\":false,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"anonymous\":false,\"type\":\"event\"}],\"languageVersion\":\"0\",\"language\":\"Solidity\",\"compilerVersion\":\"0.3.6-ebccb316\",\"source\":\"contract Token {     address issuer;     mapping (address => uint) balances;      event Issue(address account, uint amount);     event Transfer(address from, address to, uint amount);      function Token() {         issuer = msg.sender;     }      function issue(address account, uint amount) {         if (msg.sender != issuer) throw;         balances[account] += amount;     }      function transfer(address to, uint amount) {         if (balances[msg.sender] < amount) throw;          balances[msg.sender] -= amount;         balances[to] += amount;          Transfer(msg.sender, to, amount);     }      function getBalance(address account) constant returns (uint) {         return balances[account];     } }\"}}}]");
        HttpClientSevice.postJsonObject("http://localhost:9999/common/buildDeployInfo", buildDeployInfoReq);
    }

    @org.junit.Test
    public void test5() throws Exception {
//        String a = "fb1e690986a98db4d53f76f745b3eee7017b5d90671f65992a3a1c171639dc60";
//        logger.info("length:{}",a.length());
//        BuildSendInfoReq buildSendInfoReq = new BuildSendInfoReq();
//        buildSendInfoReq.setBizId("3197");
//        buildSendInfoReq.setBlockLimit("39332");
//        buildSendInfoReq.setParams(new Object[]{"0x75719430dab89c81e077007dc3a7f1292608c593", 11});
//        buildSendInfoReq.setMethod("issue");
//        buildSendInfoReq.setContractAddress("286537de4d492de31a94245c874ce5d54ff7bfb657c24d62c13d3779aba91b74");
//        HttpClientSevice.postJsonObject("http://localhost:9999/common/buildSendInfo", buildSendInfoReq);
        SignReq signReq = new SignReq();
        signReq.setMessageHash("b55ae1d3713fb4482cb2797896a8b04f495ef0c710fe2ea7a03282cacb8bf142");
        HttpClientSevice.postJsonObject("http://localhost:9999/common/sign",signReq);
    }

    @org.junit.Test
    public void test6() throws Exception {
        DeployReq deployReq = new DeployReq();
        deployReq.setSignatureInBase64("G5bWqsVRriwUU0DtduUkChcTZkX5nHpGtaEBGym7YWBpbhDy1cwRI6VQl4g990WZPE5un67N1C248X2Z7osehVc");
        deployReq.setBizId("3197");
        deployReq.setBlockLimit("40000");
        deployReq.setCode("0x60606040525b33600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908302179055505b6102c48061003f6000396000f360606040526000357c010000000000000000000000000000000000000000000000000000000090048063867904b41461004f578063a9059cbb14610070578063f8b2cb4f146100915761004d565b005b61006e60048080359060200190919080359060200190919050506100bd565b005b61008f600480803590602001909190803590602001909190505061015a565b005b6100a76004808035906020019091905050610286565b6040518082815260200191505060405180910390f35b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561011957610002565b80600160005060008473ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828282505401925050819055505b5050565b80600160005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005054101561019657610002565b80600160005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282825054039250508190555080600160005060008473ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828282505401925050819055507fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef338383604051808473ffffffffffffffffffffffffffffffffffffffff1681526020018373ffffffffffffffffffffffffffffffffffffffff168152602001828152602001935050505060405180910390a15b5050565b6000600160005060008373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000505490506102bf565b91905056");
        deployReq.setParams(new Object[0]);
        deployReq.setJsonAbi("[{\"outputs\":[],\"constant\":false,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"issue\",\"type\":\"function\"},{\"outputs\":[],\"constant\":false,\"inputs\":[{\"name\":\"to\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"type\":\"function\"},{\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"constant\":true,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"}],\"name\":\"getBalance\",\"type\":\"function\"},{\"inputs\":[],\"type\":\"constructor\"},{\"inputs\":[{\"indexed\":false,\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Issue\",\"anonymous\":false,\"type\":\"event\"},{\"inputs\":[{\"indexed\":false,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"anonymous\":false,\"type\":\"event\"}],\"languageVersion\":\"0\",\"language\":\"Solidity\",\"compilerVersion\":\"0.3.6-ebccb316\",\"source\":\"contract Token {     address issuer;     mapping (address => uint) balances;      event Issue(address account, uint amount);     event Transfer(address from, address to, uint amount);      function Token() {         issuer = msg.sender;     }      function issue(address account, uint amount) {         if (msg.sender != issuer) throw;         balances[account] += amount;     }      function transfer(address to, uint amount) {         if (balances[msg.sender] < amount) throw;          balances[msg.sender] -= amount;         balances[to] += amount;          Transfer(msg.sender, to, amount);     }      function getBalance(address account) constant returns (uint) {         return balances[account];     } }\"}}}]");
        HttpClientSevice.postJsonObject("http://localhost:9999/common/deploy",deployReq);
    }

    @org.junit.Test
    public void test7() throws Exception {
        SendTxServiceImpl sendTxService = new SendTxServiceImpl();
        CommonResult commonResult = sendTxService.blockNumber();
        logger.info("========blockNumber:{}",commonResult.getDetail());
    }

    @org.junit.Test
    public void test8() throws Exception {
        SendTxServiceImpl sendTxService = new SendTxServiceImpl();
        CompileSolidityReq compileSolidityReq = new CompileSolidityReq();
        compileSolidityReq.setSource("contract Token {   " +
                "  address issuer;   " +
                "  mapping (address => uint) balances;   " +
                "   event Issue(address account, uint amount);  " +
                "   event Transfer(address from, address to, uint amount);   " +
                "   function Token() {    " +
                "     issuer = msg.sender;  " +
                "   }   " +
                "   function issue(address account, uint amount) {     " +
                "    if (msg.sender != issuer) throw;     " +
                "    balances[account] += amount;     }   " +
                "   function transfer(address to, uint amount) {    " +
                "     if (balances[msg.sender] < amount) throw;    " +
                "      balances[msg.sender] -= amount;    " +
                "     balances[to] += amount;     " +
                "     Transfer(msg.sender, to, amount);     }   " +
                "   function getBalance(address account) constant returns (uint) {    " +
                "     return balances[account];     } }");
        sendTxService.compileSolidity(compileSolidityReq);
    }

    @org.junit.Test
    public void test9() throws Exception {
        SendTxServiceImpl sendTxService = new SendTxServiceImpl();
        String commonResult = HttpClientSevice.get("http://localhost:9999/common/blockNumberDB");
        logger.info("commonResult:{}", commonResult);
        CommonResult commonResult1 = JSON.parseObject(commonResult, new com.alibaba.fastjson.TypeReference<CommonResult>() {
        });
        Integer currentBlockNumber = (Integer) commonResult1.getDetail();
        Integer blockLinit = currentBlockNumber + 100;
        logger.info("commonResult1:{}", commonResult1.toString());
        BuildDeployInfoReq buildDeployInfoReq = new BuildDeployInfoReq();
        buildDeployInfoReq.setBizId((int) (Math.random() * 10000) + "");
        buildDeployInfoReq.setBlockLimit(blockLinit.toString());
        buildDeployInfoReq.setCode("0x60606040525b33600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908302179055505b6102c48061003f6000396000f360606040526000357c010000000000000000000000000000000000000000000000000000000090048063867904b41461004f578063a9059cbb14610070578063f8b2cb4f146100915761004d565b005b61006e60048080359060200190919080359060200190919050506100bd565b005b61008f600480803590602001909190803590602001909190505061015a565b005b6100a76004808035906020019091905050610286565b6040518082815260200191505060405180910390f35b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561011957610002565b80600160005060008473ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828282505401925050819055505b5050565b80600160005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005054101561019657610002565b80600160005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282825054039250508190555080600160005060008473ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828282505401925050819055507fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef338383604051808473ffffffffffffffffffffffffffffffffffffffff1681526020018373ffffffffffffffffffffffffffffffffffffffff168152602001828152602001935050505060405180910390a15b5050565b6000600160005060008373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000505490506102bf565b91905056");
        buildDeployInfoReq.setParams(new Object[0]);
        buildDeployInfoReq.setJsonAbi("[{\"outputs\":[],\"constant\":false,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"issue\",\"type\":\"function\"},{\"outputs\":[],\"constant\":false,\"inputs\":[{\"name\":\"to\",\"type\":\"address\"},{\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"type\":\"function\"},{\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"constant\":true,\"inputs\":[{\"name\":\"account\",\"type\":\"address\"}],\"name\":\"getBalance\",\"type\":\"function\"},{\"inputs\":[],\"type\":\"constructor\"},{\"inputs\":[{\"indexed\":false,\"name\":\"account\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Issue\",\"anonymous\":false,\"type\":\"event\"},{\"inputs\":[{\"indexed\":false,\"name\":\"from\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"anonymous\":false,\"type\":\"event\"}],\"languageVersion\":\"0\",\"language\":\"Solidity\",\"compilerVersion\":\"0.3.6-ebccb316\",\"source\":\"contract Token {     address issuer;     mapping (address => uint) balances;      event Issue(address account, uint amount);     event Transfer(address from, address to, uint amount);      function Token() {         issuer = msg.sender;     }      function issue(address account, uint amount) {         if (msg.sender != issuer) throw;         balances[account] += amount;     }      function transfer(address to, uint amount) {         if (balances[msg.sender] < amount) throw;          balances[msg.sender] -= amount;         balances[to] += amount;          Transfer(msg.sender, to, amount);     }      function getBalance(address account) constant returns (uint) {         return balances[account];     } }\"}}}]");
       sendTxService.buildDeployInfo(buildDeployInfoReq);
    }
}
