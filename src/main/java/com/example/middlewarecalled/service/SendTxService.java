package com.example.middlewarecalled.service;

import com.example.middlewarecalled.bean.*;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 发送交易所需的业务接口
 *
 * @author daiyi
 */
public interface SendTxService {
    //取块高度
    CommonResult blockNumber();
    //编译合约
    CommonResult compileSolidity( CompileSolidityReq compileSolidityReq);
    //构造一个部署合约的区块链请求
    CommonResult buildDeployInfo( BuildDeployInfoReq buildDeployInfoReq);
    //签名
    CommonResult sign( SignReq signReq);
    //发送部署信息
    CommonResult deploy( DeployReq deployReq);
    //发送交易及签名信息
    CommonResult sendTx( SendTxReq sendTxReq);
}
