package com.example.middlewarecalled.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.middlewarecalled.bean.*;
import com.example.middlewarecalled.service.HttpClientSevice;
import com.example.middlewarecalled.service.HttpException;
import com.example.middlewarecalled.service.SendTxService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;
@Service
public class SendTxServiceImpl implements SendTxService {
    
    private static final Logger logger = getLogger(SendTxServiceImpl.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    
    @Override
    public CommonResult blockNumber() {
        CommonResult commonResult = new CommonResult();
        String blockNumber = null;
        try {
            blockNumber = HttpClientSevice.get("http://localhost:9999/common/blockNumber");
            commonResult = JSON.parseObject(blockNumber, new com.alibaba.fastjson.TypeReference<CommonResult>() {
            });
            logger.info("blockNumber:{}",commonResult.getDetail());
        } catch (HttpException e) {
            logger.info("blockNumber is error:{}",commonResult.getDetail());
        } catch (IOException e) {
            logger.info("blockNumber is error:{}",commonResult.getDetail());
        }
        return commonResult;
    }

    @Override
    public CommonResult compileSolidity(CompileSolidityReq compileSolidityReq) {
        String result = null;
        CommonResult<Map<String,CompileResult>> commonResult = new CommonResult();
        try {
            result = HttpClientSevice.postJsonObject("http://localhost:9999/common/compileSolidity", compileSolidityReq);
            logger.info("commonResult:{}", result);
            commonResult = mapper.readValue(result,new  TypeReference<CommonResult<Map<String,CompileResult>>>(){});
            logger.info("commonResult:{}", commonResult.getDetail());
            logger.info("==========================commonResult:{}", commonResult.getDetail().get("Token").getAbiJson());
        } catch (HttpException e) {
            logger.info("compileSolidity is error:{}",commonResult.getDetail());
        } catch (IOException e) {
            logger.info("compileSolidity is error:{}",commonResult.getDetail());
        }
        return commonResult;
    }

    @Override
    public CommonResult buildDeployInfo(BuildDeployInfoReq buildDeployInfoReq) {
        String result = null;
        CommonResult commonResult = new CommonResult();
        try {
            result = HttpClientSevice.postJsonObject("http://localhost:9999/common/buildDeployInfo", buildDeployInfoReq);
            commonResult = mapper.readValue(result,new  TypeReference<CommonResult<String>>(){});
            logger.info("commonResult:{}", commonResult.getDetail());
        } catch (HttpException e) {
            logger.info("buildDeployInfo is error:{}",commonResult.getDetail());
        } catch (IOException e) {
            logger.info("buildDeployInfo is error:{}",commonResult.getDetail());
        }
        return commonResult;
    }

    @Override
    public CommonResult sign(SignReq signReq) {
        String result = null;
        CommonResult commonResult = new CommonResult();
        try {
            result = HttpClientSevice.postJsonObject("http://localhost:9999/common/sign", signReq);
            commonResult = mapper.readValue(result,new  TypeReference<CommonResult<String>>(){});
            logger.info("commonResult:{}", commonResult.getDetail());
        } catch (HttpException e) {
            logger.info("sign is error:{}",commonResult.getDetail());
        } catch (IOException e) {
            logger.info("sign is error:{}",commonResult.getDetail());
        }
        return commonResult;
    }

    @Override
    public CommonResult deploy(DeployReq deployReq) {
        String result = null;
        CommonResult commonResult = new CommonResult();
        try {
            result = HttpClientSevice.postJsonObject("http://localhost:9999/common/deploy", deployReq);
            commonResult = mapper.readValue(result,new  TypeReference<CommonResult<String>>(){});
            logger.info("commonResult:{}", commonResult.getDetail());
        } catch (HttpException e) {
            logger.info("deploy is error:{}",commonResult.getDetail());
        } catch (IOException e) {
            logger.info("deploy is error:{}",commonResult.getDetail());
        }
        return commonResult;
    }

    @Override
    public CommonResult sendTx(SendTxReq sendTxReq) {
        String result = null;
        CommonResult commonResult = new CommonResult();
        try {
            result = HttpClientSevice.postJsonObject("http://localhost:9999/common/sendTx", sendTxReq);
            commonResult = mapper.readValue(result,new  TypeReference<CommonResult<String>>(){});
            logger.info("commonResult:{}", commonResult.getDetail());
        } catch (HttpException e) {
            logger.info("sendTx is error:{}",commonResult.getDetail());
        } catch (IOException e) {
            logger.info("sendTx is error:{}",commonResult.getDetail());
        }
        return commonResult;
    }
}
