package com.example.middlewarecalled.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.*;
import org.apache.http.annotation.Contract;
import org.apache.http.annotation.ThreadingBehavior;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

//@Service
@Contract(threading = ThreadingBehavior.SAFE)
public class HttpClientSevice {

    private static final Logger logger = getLogger(HttpClientSevice.class);

    /**
     * Post JSON对象
     *
     * @param url
     * @param value
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    public static String postJsonObject(String url, Object jsonObj) throws ParseException, HttpException, IOException {
        logger.info("post json object, url:{}, json object:{}", url, jsonObj);

        String json = new ObjectMapper().writeValueAsString(jsonObj);

        return postJsonString(url, json);
    }

    /**
     * post JOSN字符串
     *
     * @param url
     * @param jsonStr
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    public static String postJsonString(String url, String jsonStr) throws ParseException, HttpException, IOException {
        logger.info("post json String, url:{}, json String:{}", url, jsonStr);

        HttpPost httpPost = new HttpPost(url);

        StringEntity params = new StringEntity(jsonStr, "UTF-8");
        params.setContentType("application/json");
        params.setContentEncoding("UTF-8");
        httpPost.setEntity(params);

        return post(httpPost);
    }

    /**
     * Post Key-Value (map)
     *
     * @param url
     * @param map
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    public static String postMap(String url, Map<String, String> map) throws ParseException, HttpException, IOException {
        logger.info("post key-value map, url:{}, map:{}", url, map);

        HttpPost httpPost = new HttpPost(url);
        if (map != null) {
            List<NameValuePair> pair = new ArrayList<NameValuePair>();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                pair.add(new BasicNameValuePair(entry.getKey(), entry
                        .getValue()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(pair));
        }

        return post(httpPost);
    }

    /**
     * post HttpServletRequest
     *
     * @param url
     * @param request
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    public static String postRequest(String url, HttpServletRequest request) throws ParseException, HttpException, IOException {
        logger.info("post http servlet request, url:{}, contentType:{}", url,
                request.getContentType());

        HttpPost httpPost = new HttpPost(url);

        HttpEntity entity = new InputStreamEntity(request.getInputStream(),
                request.getContentLength());
        httpPost.setEntity(entity);

        return post(httpPost);
    }

    /**
     * post InputStream
     *
     * @param url
     * @param header
     * @param in
     * @param size
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    public static String postStream(String url, Map<String, String> header,
                                    InputStream in, int size) throws ParseException, HttpException, IOException {
        logger.info("post http InputStream, url:{}, header:{}", url, header);

        HttpPost httpPost = new HttpPost(url);

        HttpEntity entity = new InputStreamEntity(in, size);
        for (Map.Entry<String, String> entry : header.entrySet()) {
            httpPost.addHeader(entry.getKey(), entry.getValue());
        }
        httpPost.setEntity(entity);

        return post(httpPost);
    }

    /**
     * get
     *
     * @param url
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    public static String get(String url) throws ParseException, HttpException, IOException {
        logger.info("get url {}", url);

        HttpGet get = new HttpGet(url);

        try (CloseableHttpClient client = HttpClients.createSystem()) {
            HttpResponse response = client.execute(get);

            return dealResponse(response);
        }
    }

    /**
     * post HttpPost
     *
     * @param httpPost
     * @return
     * @throws IOException
     * @throws HttpException
     * @throws ParseException
     */
    private static String post(HttpPost httpPost) throws ParseException, HttpException, IOException {
        try (CloseableHttpClient client = HttpClients.createSystem()) {
            HttpResponse response = client.execute(httpPost);

            return dealResponse(response);
        }
    }

    /**
     * 解析数据 HttpResponse
     *
     * @param response
     * @return
     * @throws HttpException
     * @throws IOException
     * @throws ParseException
     */
    private static String dealResponse(HttpResponse response) throws HttpException, ParseException, IOException {

        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            HttpEntity entity = response.getEntity();

            String content = EntityUtils.toString(entity);
            logger.info("http response {}", content);

            return content;
        } else {
            logger.warn("http get or post result, status line:{}",
                    response.getStatusLine());
            throw new HttpException(response.getStatusLine().toString());
        }

    }
}
