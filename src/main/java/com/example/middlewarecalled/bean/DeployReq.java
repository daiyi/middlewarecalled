package com.example.middlewarecalled.bean;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class DeployReq {

    private String bizId;

    private String blockLimit;

    private String code;

    private Object[] params;

    private String signatureInBase64;

    private String jsonAbi;

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public String getBlockLimit() {
        return blockLimit;
    }

    public void setBlockLimit(String blockLimit) {
        this.blockLimit = blockLimit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSignatureInBase64() {
        return signatureInBase64;
    }

    public void setSignatureInBase64(String signatureInBase64) {
        this.signatureInBase64 = signatureInBase64;
    }

    public String getJsonAbi() {
        return jsonAbi;
    }

    public void setJsonAbi(String jsonAbi) {
        this.jsonAbi = jsonAbi;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public Object[] getParams() {
        return params;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

}
