package com.example.middlewarecalled.bean;

import com.google.common.base.MoreObjects;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;

import java.io.IOException;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by houdg on 16/8/5.
 */
public class CompileResult {

    private static final Logger logger = getLogger(CompileResult.class);

    private String code;
    private Info info;
    private String abi;//
    private String abiJson;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    /**
     * 以 JSON 形式返回 ABI
     *
     * @return API
     * @throws IOException 
     */
    public String getAbiJson() throws IOException {
        if (info == null) throw new RuntimeException("CompileResult.info is null, can not load ABI");

        String abiJson;
        if (info.getAbiDefinition() == null) {
            logger.info("trying to get a null abiJson from {}", info);
            abiJson = "[]";     // return empty list to void NullPointerException
        } else if(abi != null){
        	abiJson = abi;
        } else {
            abiJson = new ObjectMapper().writeValueAsString(info.getAbiDefinition());
        }

        return abiJson;
    }

    public void setAbi(String abi){
    	if(info == null){
    		info = new Info();
    	}
    	info.setAbiDefinition(abi);
    	this.abi = abi;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", code)
                .add("info", info)
                .toString();
    }

    /**
     * 编译结果的 info 字段结构体
     */
    public static class Info {
        private String source;
        private String language;
        private String languageVersion;
        private String compilerVersion;
        private String compilerOptions;
        private Object abiDefinition;
        private Object userDoc;
        private Object developerDoc;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getLanguageVersion() {
            return languageVersion;
        }

        public void setLanguageVersion(String languageVersion) {
            this.languageVersion = languageVersion;
        }

        public String getCompilerVersion() {
            return compilerVersion;
        }

        public void setCompilerVersion(String compilerVersion) {
            this.compilerVersion = compilerVersion;
        }

        public String getCompilerOptions() {
            return compilerOptions;
        }

        public void setCompilerOptions(String compilerOptions) {
            this.compilerOptions = compilerOptions;
        }

        public Object getAbiDefinition() {
            return abiDefinition;
        }

        public void setAbiDefinition(Object abiDefinition) {
            this.abiDefinition = abiDefinition;
        }

        public Object getUserDoc() {
            return userDoc;
        }

        public void setUserDoc(Object userDoc) {
            this.userDoc = userDoc;
        }

        public Object getDeveloperDoc() {
            return developerDoc;
        }

        public void setDeveloperDoc(Object developerDoc) {
            this.developerDoc = developerDoc;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("source", source)
                    .add("language", language)
                    .add("languageVersion", languageVersion)
                    .add("compilerVersion", compilerVersion)
                    .add("compilerOptions", compilerOptions)
                    .add("abiDefinition", abiDefinition)
                    .add("userDoc", userDoc)
                    .add("developerDoc", developerDoc)
                    .toString();
        }
    }
}
