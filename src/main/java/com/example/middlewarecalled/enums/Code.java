package com.example.middlewarecalled.enums;

public enum Code {

    SUCCESS("运行成功", 200),

    SERVER_ERROR("服务器内部错误", 420),

    PARAM_ERROR("参数错误", 421),

    FILE_NOT_FOUND("找不到文件", 404),

    INVALID_FILE_ID("fileId无效", 400),

    FILE_TOO_LARGE("超过了文件大小限制", 400),

    BIZ_ID_NOT_FOUND("bizId不存在", 400),

    INVALID_SIGNATURE("签名错误", 400),

    VERIFY_ERROR("检查数据错误", 422),

    SEARCH_SYNTAX_ERROR("查询语法错误", 400);;

    private String message;
    private int status;

    Code(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }
}
