package com.example.middlewarecalled.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
     * Created by zccp on 2017/6/20.
     */
    @RestController
    @RequestMapping("/")
    public class TestController {

        /**
         * 该方法为进入index.html的方法
         * @return
         */
        @RequestMapping("/")
        public String toRegister(){

            System.out.println("toRegister");
            return "index";
        }



}
